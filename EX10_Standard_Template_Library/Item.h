#pragma once

#include<iostream>
#include<string>
#include<algorithm>


#define SERIAL_NUM_SIZE 5


class Item
{
public:
	Item(const std::string& name, const std::string& serialNumber, const double unitPrice);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.
	friend std::ostream& operator<<(std::ostream& os, const Item& item);

	//setters
	void setName(const std::string& name);
	void setSerialNumber(const std::string& serialNumber);
	void setUnitPrice(const int unitPrice);
	void setCount(const int count);

	//getters
	std::string getName() const;
	std::string getSerialNumber() const;
	int getUnitPrice() const;
	int getCount() const;

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};

