#include "Customer.h"


/*
The function constructs the current Customer class according to given variables
Input: string name of the customer
*/
Customer::Customer(const std::string& name):
	_name(name)
{
}

Customer::Customer():
	_name("")
{
}


/*
The function deconstructs the current Customer class
*/
Customer::~Customer()
{
	this->_items.clear();  //cleaers the set container that is being used in the current class
}


/*
The function calculates the total price of all the items inside the class set container
Output: the total price of all the items inside the class set conatainer
*/
double Customer::totalSum() const
{
	double sum = 0;

	std::set<Item>::iterator currItem;
	for (currItem = this->_items.begin(); currItem != this->_items.end(); ++currItem)  //the loop runs on the set container items 
	{
		sum += currItem->totalPrice();  //adds each item's price to the total sum
	}

	return sum;
}


/*
The function adds the given item to the current class set container
Input: an item to add to the set container
*/
void Customer::addItem(const Item& item)
{
	std::set<Item>::iterator currItem = this->_items.find(item);  //uses the find function to search for the given item

	if (currItem != this->_items.end())  //if the given item has been found, it will update the item in the set container
	{
		//creates a copy of the given item according to whats in the set container, and increases count by one
		Item copyItem = Item(currItem->getName(), currItem->getSerialNumber(), currItem->getUnitPrice());
		copyItem.setCount(currItem->getCount() + 1); 

		this->_items.erase(item);  //erases the given item from the list
		this->_items.insert(copyItem);  //inserts the created item back to the set conatainer
	}
	else  //else just inserts it regulary 
	{
		this->_items.insert(item);
	} 
}


/*
The function removes the given item from the current class set container
Input: an item to remove from the set container
*/
void Customer::removeItem(const Item& item)
{
	std::set<Item>::iterator currItem = this->_items.find(item);  //uses the find function to search for the given item

	if (currItem != this->_items.end())  //if the given item has been found, it will update the item in the set container
	{
		//creates a copy of the given item according to whats in the set container, and decreases count by one
		Item copyItem = Item(currItem->getName(), currItem->getSerialNumber(), currItem->getUnitPrice());
		copyItem.setCount(currItem->getCount() - 1);

		this->_items.erase(item);  //erases the given item from the list

		if (copyItem.getCount() != 0)  //if its count is more than 0, it will add it back with the its new count
		{
			this->_items.insert(copyItem);
		}
	}
}


/*
The function changes the name variable of the current class according to the given string name
Input: a string name to put inside the current class
*/
void Customer::setName(const std::string& name)
{
	this->_name = name;
}


/*
The function changes the items set container variable of the current class according to the given set container items
Input: a set container to put inside the current class
*/
void Customer::setItems(const std::set<Item>& items)
{
	this->_items = items;
}


/*
The function returns the current class name variable
*/
std::string Customer::getName() const
{
	return this->_name;
}


/*
The function returns the current class items variable
*/
std::set<Item> Customer::getItems() const
{
	return this->_items;
}

