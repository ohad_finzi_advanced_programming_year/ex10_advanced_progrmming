#pragma once

#include"Item.h"
#include<set>


class Customer
{
public:
	Customer(const std::string& name);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(const Item& item);//add item to the set
	void removeItem(const Item& item);//remove item from the set

	//setters
	void setName(const std::string& name);
	void setItems(const std::set<Item>& items);

	//getters
	std::string getName() const;
	std::set<Item> getItems() const;

private:
	std::string _name;
	std::set<Item> _items;

};

