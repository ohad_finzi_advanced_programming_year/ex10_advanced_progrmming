#include"Customer.h"
#include<map>


//global variables
std::map<std::string, Customer> abcCustomers;  //a map which concludes all the customers 
const Item itemList[10] = //an array which concludes all the item that are in the MagshiMart
{  
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) 
};


//project functions
void runMagshiMart();
std::string printMainMenu();
std::string printOption2Menu();
void printItems();
bool searchName(const std::string& name);
void printDetails(const std::string& name);
void removeCustomerItems(const std::string& name);
void option1(const std::string& name);
void option2(const std::string& name);
void option3();


int main()
{
	runMagshiMart();

	return 0;
}


/*
The function runs the super market of magshimim
it activates all the project functions in the right sequence and time according to the user input
*/
void runMagshiMart()
{
	std::string option = "";
	std::string currName = "";
	std::map<std::string, Customer>::iterator it;

	while (option != "4")  //a loop which runs until the user enters the number 4 which exits the market
	{
		try
		{
			option = printMainMenu();  //prints the main menu and gets the input from the user

			if (option == "1")  //if the user entered the number 1, it activates option1 which creates a new customer and receives its basket with grocerires
			{
				std::cout << "Please enter a name to add to the customer list: ";
				std::cin >> currName;

				if (!searchName(currName))  //if the given name is not registered in the customers list
				{
					abcCustomers[currName] = Customer(currName);  //creates the new customer according to the given name
					option1(currName);  //activates the option1 functions sequence
				}
				else  //if the given name is already registeres to the customers list
				{
					throw std::string("Invalid name! The given name is already in the customer list");
				}
			}
			else if (option == "2")  //if the user entered the number 2, it activates option2 which updates a certain customers out of the customers list according to the input from the user
			{
				std::cout << "Please enter a name from the customer list to update: ";
				std::cin >> currName;

				if (searchName(currName))  //if the given name is registerd in the customers list
				{
					option2(currName);  //activates the option2 function sequence
				}
				else  //if the given name is not registered in the customers list
				{
					throw std::string("Invalid name! The given name is not found in the customer list");
				}
			}
			else if (option == "3")  //if the user entered the number 3, it activates option2 which prints the details about the customer who spent the most money from the customers list
			{
				option3();  //activates the option3 function sequence
			}
			else if (option == "4")  
			{
				std::cout << "BYEBYE" << std::endl;
			}
			else   //incase the user enters an invalid option
			{
				throw std::string("Invalid option! Accepts only numbers between 1 and 4");
			}
		}
		catch (const std::string& error)  //to catch all the types of throws which accuring along the project 
		{
			std::cout << error << std::endl;
		}
	}
}


/*
The function prints the main menu to the user and gets his option
Output: a string which symbolizes the user main menu option
*/
std::string printMainMenu()
{
	std::string option = "";

	std::cout << "Welcome to MagshiMart!" << std::endl;
	std::cout << "1. to sign as customer and buy items" << std::endl;
	std::cout << "2. to uptade existing customer's items" << std::endl;
	std::cout << "3. to print the customer who pays the most" << std::endl;
	std::cout << "4. to exit" << std::endl;
		
	std::cin >> option;

	return option;
}


/*
The function prints the option2 menu to the user and gets his option
Output: a string which symbolizes the user option2 menu option
*/
std::string printOption2Menu()
{
	std::string option = "";

	std::cout << "1. Add items" << std::endl;
	std::cout << "2. Remove items" << std::endl;
	std::cout << "3. Back to menu" << std::endl;

	std::cin >> option;

	return option;
}


/*
The function prints the items available to purchase in the magshimim super market
*/
void printItems()
{
	int i = 0;

	for (i = 0; i < sizeof(itemList) / sizeof(itemList[0]); i++)  //runs on the global item list elements
	{
		//prints the list items
		std::cout << (i + 1) << ". ";
		std::cout << itemList[i] << std::endl;
	}
}


/*
The function searches for the given name inside the global customers map list
Input: a string which symbolizes the name to search
Ouput: true if the given name is inside the customers list and false if not
*/
bool searchName(const std::string& name)
{
	bool ans = false;

	std::map<std::string, Customer>::iterator currItem = abcCustomers.find(name);  //uses the find function to search for the given item
	if (currItem != abcCustomers.end())  //if the given item has been found, the ans will be changed to true
	{
		ans = true;
	}

	return ans;
}


/*
The function prints the list items of the given name by using the global customers map list
Input: a string which symbolizes the name of the customer to print its list items
*/
void printDetails(const std::string& name)
{
	std::set<Item> items = abcCustomers[name].getItems();  //gets the list of the given customer

	//prints the whole list using the ostream iterator
	std::cout << "Customer name: " << name << std::endl;
	std::ostream_iterator<Item> out_it(std::cout, "\n");
	std::copy(items.begin(), items.end(), out_it);
}


/*
The function prints the list items of the given name, and asks from the user to remove any of them
Input: a string which symoblizes the name of the customer
*/
void removeCustomerItems(const std::string& name)
{
	int option = 1;
	std::set<Item> userList = abcCustomers[name].getItems();
	std::set<Item>::iterator currItem = userList.begin();

	std::cout << "The items you can remove are: (0 to exit)" << std::endl;
	printDetails(name);
	while (option != 0)  //runs until the user will enter the number 0 
	{
		std::cout << "What items would you like to remove?" << std::endl;
		std::cin >> option;

		if (option >= 1 && option <= userList.size())  //if the user entered a number inside the bounderies of his list
		{
			std::advance(currItem, option - 1);  //gets the iterator to point on the wanted item to remove
			abcCustomers[name].removeItem(*currItem);  //removes the wanted item
		}
		else if (std::cin.fail() || option != 0)  //incase the user entered an invalid option or if he cause the cin to fail
		{
			//ignores the wrong input
			std::cin.clear();
			std::cin.ignore();

			std::cout << "Invalid option! Enter numbers between 0 and " << userList.size() << std::endl;
		}
	}
}


/*
The function performs the option1 from the task
it prints the list of available items to the user and asks him to add any of them to his list
Input: a string which symbolizes the name of the customer
*/
void option1(const std::string& name)
{
	int option = 1;

	std::cout << "The items you can buy are: (0 to exit)" << std::endl;
	printItems();
	while (option != 0)  //runs until the user will enter the number 0 
	{
		std::cout << "What items would you like to buy?" << std::endl;
		std::cin >> option;

		if (option >= 1 && option <= 10)  //if the user entered a number inside the bounderies of the global items list (which concludes all the avaliable items to the customers)
		{
			abcCustomers[name].addItem(itemList[option - 1]);  //adds the wanted item to the user list
		}
		else if(std::cin.fail() || option != 0)  //incase the user entered an invalid option or if he cause the cin to fail
		{
			//ignores the wrong input
			std::cin.clear();
			std::cin.ignore();

			std::cout << "Invalid option! Enter numbers between 0 and 10" << std::endl;
		}
	}
}


/*
The function performs the option2 from the task
it updated the given customer according to the user input
Input: a string which symbolizes the name of the customer
*/
void option2(const std::string& name)
{
	std::string option = "";

	while (option != "3")  //runs until the user will enter the number 3
	{
		option = printOption2Menu();  //prints the option2 menu and gets the input from the user

		if (option == "1")  //if the user entered the number 1, it means that he want to add more items to his list
		{
			option1(name);  //activaes the option1 function which let the user to add any item he want to his list
		}
		else if (option == "2")  //if the user enetered the number 2, it means that he want to remove items from his list
		{
			removeCustomerItems(name); 
		}
		else if (option == "3")  //if the user eneted the number 3, it means he want to return to the main menu
		{
			std::cout << "Returning to menu..." << std::endl;
		}
		else  //incase the user entered an invalid option
		{
			std::cout << "Invalid option! Enter numbers between 1 and 3" << std::endl;
		}
	}
}


/*
The function performs the option3 from the task
it prints the item list of the customer who spent the most money out of the whole customer list
*/
void option3()
{
	std::string name = "";
	double biggestSpend = 0;
	double currAmount = 0;

	std::map<std::string, Customer>::iterator currCustomer;
	for (currCustomer = abcCustomers.begin(); currCustomer != abcCustomers.end(); ++currCustomer)  //runs on the global customer list using iterators
	{
		currAmount = currCustomer->second.totalSum();  //gets the total sum of the current user list
		if (currAmount >= biggestSpend)  //checks if it bigger from the biggest total sum list
		{
			biggestSpend = currAmount;  //updates the biggest total sum list
			name = currCustomer->second.getName();  //updates the name of the customer with the biggest total sum list
		}
	}

	printDetails(name);  //prints the user details
}

