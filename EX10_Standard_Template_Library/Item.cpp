#include "Item.h"


/*
The function constructs the current Item class according to the given variables
Input: string name, string serial number, double unit price
*/
Item::Item(const std::string& name, const std::string& serialNumber, const double unitPrice) :
	_name(name), _count(1)
{
	this->setSerialNumber(serialNumber);  //to check the correction of the given serial number
	this->setUnitPrice(unitPrice);  //to check the correction of the given unit price
}


/*
The function decontructs the current Item class
*/
Item::~Item()
{
	//no dynamic memory allocated in this class
}


/*
The function returns the total price of the current Item class by calculating count multiply by unit price
Output: count multiply by unit price
*/
double Item::totalPrice() const
{
	return (this->_count * this->_unitPrice);
}


/*
The function overrides the operator < when using the current class
Input: other Item class
Output: true if the current class elements are smaller than the given Item class and false if not
*/
bool Item::operator<(const Item& other) const
{
	return (this->_serialNumber < other.getSerialNumber());
}


/*
The function overrides the operator > when using the current class
Input: other Item class
Output: true if the current class elements are bigger than the given Item class and false if not
*/
bool Item::operator>(const Item& other) const
{
	return (this->_serialNumber > other.getSerialNumber());
}


/*
The function overrides the operator == when using the current class
Input: other Item class
Output: true if the current class elements are equal to the given Item class and false if not
*/
bool Item::operator==(const Item& other) const
{
	return (this->_serialNumber == other.getSerialNumber());
}


/*
The funcion sets the current class name variable according to the given name 
Input: string to put in the current class name variable
*/
void Item::setName(const std::string& name)
{
	this->_name = name;
}


/*
The funcion sets the current class serial number variable according to the given serial number
Input: string to put in the current class serial number variable
*/
void Item::setSerialNumber(const std::string& serialNumber)
{
	if (serialNumber.length() == SERIAL_NUM_SIZE)   //if the given serial number is in the length of 5.
	{
		for (int i = 0; i < SERIAL_NUM_SIZE; i++)
		{
			if (!isdigit(serialNumber[i]))  //if the serial number has a char which is not a number
			{
				throw std::string("Invalid serial number of item! Can only include numbers");
			}
		}
	}
	else  //if the given serial number isnt in the lenght of 5
	{
		throw std::string("Invalid serial number of item! Can only be at the length of 5");
	}

	this->_serialNumber = serialNumber;  //sets the given serial number if its legal
}


/*
The funcion sets the current class unit price variable according to the given unit price
Input: int to put in the current class unit price variable
*/
void Item::setUnitPrice(const int unitPrice)
{
	if (unitPrice <= 0)
	{
		throw std::string("Invalid price of item!");  //causes an exception if the given unit price is legal - price above 0
	}

	this->_unitPrice = unitPrice;  //sets the given unit price if its legal
}


/*
The funcion sets the current class count variable according to the given count
Input: string to put in the current class count variable
*/
void Item::setCount(const int count)
{
	this->_count = count;
}


/*
The function returns the current class string name varialbe
*/
std::string Item::getName() const
{
	return this->_name;
}


/*
The function returns the current class string serial number varialbe
*/
std::string Item::getSerialNumber() const
{
	return this->_serialNumber;
}


/*
The function returns the current class int unit price varialbe
*/
int Item::getUnitPrice() const
{
	return this->_unitPrice;
}


/*
The function returns the current class int count varialbe
*/
int Item::getCount() const
{
	return this->_count;
}


/*
The function overrides the << operator and prints all the given Item class variables
*/
std::ostream& operator<<(std::ostream& os, const Item& item)
{
	os << item.getName() << ",	" << item.getSerialNumber() << ",	" << item.getUnitPrice() << ",	" << item.getCount();

	return os;
}

